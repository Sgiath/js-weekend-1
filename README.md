# Kiwi - JavaScript weekend entry task

**Author:** Filip Vavera <FilipVavera@sgiath.net>

**Deployed at:**
  * https://kiwi-entry-1.firebaseapp.com/
  * https://kiwi.sgiath.net/

## Develop
  * `yarn install`
  * `yarn start`

## Notes
  * Suggestions are not properly implemented on the UI (Material Components does not support it natively and my solution with Menu component is VERY buggy). So it is turned off by default. You can turn it on with switch on the right (but be prepared for "not ideal" user experience).
  * Pagination is implemented as infinite scroll instead of actual paginated pages. I think it is cooler :)
