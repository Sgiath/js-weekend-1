import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import createSagaMiddleware from 'redux-saga';

import { Root } from './containers';
import configureStore from './configureStore';
import rootSaga from './sagas';
import registerServiceWorker from './registerServiceWorker';

import './styles.css';

// Configure history
const history = createHistory();
history.listen((location, action) => {
  // TODO: add Google Analytics code here
});

// Configure Store and Saga
const sagaMiddleware = createSagaMiddleware();
const store = configureStore(history, sagaMiddleware);
sagaMiddleware.run(rootSaga);

// Render React
ReactDOM.render(<Root store={store} history={history} />, document.getElementById('root'));

// Register Service Worker
registerServiceWorker();
