import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'rmwc/Card';
import { Typography } from 'rmwc/Typography';

class CenterCard extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
  };

  render() {
    return (
      <Card
        style={{
          maxWidth: '480px',
          marginLeft: 'auto',
          marginRight: 'auto',
          marginBottom: '50px',
          alignItems: 'center',
        }}>
        <div style={{ padding: '0 1rem 1rem 1rem' }}>
          <Typography use="title" tag="h2">
            {this.props.title}
          </Typography>
          {this.props.children}
        </div>
      </Card>
    );
  }
}

export default CenterCard;
