import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'rmwc/TextField';

class InputField extends React.Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    type: PropTypes.string,
  };

  static defaultProps = {
    type: 'text',
  };

  render() {
    return (
      <TextField
        label={this.props.label}
        value={this.props.value}
        type={this.props.type}
        onChange={(event) => this.props.onChange(event.target.value)}
      />
    );
  }
}

export default InputField;
