import React from 'react';
import PropTypes from 'prop-types';
import { LinearProgress } from 'rmwc/LinearProgress';

class DataComponent extends React.Component {
  static propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.object,
    data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    meta: PropTypes.object,
    render: PropTypes.func.isRequired,
  };

  static defaultProps = {
    loading: false,
    error: undefined,
    data: undefined,
    meta: undefined,
  };

  render() {
    const { loading, data, error, meta } = this.props;
    if (error) {
      return JSON.stringify(this.props.error);
    }
    if (loading && !data) {
      return <LinearProgress determinate={false} />;
    }
    if (!data) {
      return '';
    }
    return this.props.render(data, meta);
  }
}

export default DataComponent;
