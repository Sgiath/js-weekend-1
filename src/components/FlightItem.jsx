import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, ListItemText, ListItemSecondaryText } from 'rmwc/List';

class FlightItem extends React.Component {
  static propTypes = {
    route: PropTypes.array.isRequired,
    price: PropTypes.string.isRequired,
    duration: PropTypes.string.isRequired,
  };

  render() {
    return (
      <ListItem tag="li">
        <ListItemText>
          {this.props.route.reduce(
            (acc, value) => `${acc} -> ${value['flyTo']} (${value['cityTo']})`,
            `${this.props.route[0]['flyFrom']} (${this.props.route[0]['cityFrom']})`,
          )}
          <ListItemSecondaryText>
            Price: {this.props.price}, Duration: {this.props.duration}
          </ListItemSecondaryText>
        </ListItemText>
      </ListItem>
    );
  }
}

export default FlightItem;
