import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'rmwc/TextField';
import { Menu, MenuAnchor, MenuItem } from 'rmwc/Menu';

class InputFieldSuggestions extends React.Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    suggestions: PropTypes.array,
    onChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    suggestions: [],
  };

  render() {
    return (
      <MenuAnchor>
        <TextField
          inputRef={(ref) => (this.input = ref)}
          label={this.props.label}
          value={this.props.value}
          onChange={(event) => this.props.onChange(event.target.value)}
        />
        <Menu
          open={this.props.suggestions && this.props.suggestions.length > 1 && this.input === document.activeElement}
          anchorCorner="bottomStart"
          onSelected={(event) => {
            this.props.onChange(event.detail.item.innerText);
            this.setState({ open: false });
          }}>
          {this.props.suggestions && this.props.suggestions.map(({ id, name }) => <MenuItem key={id}>{name}</MenuItem>)}
        </Menu>
      </MenuAnchor>
    );
  }
}

export default InputFieldSuggestions;
