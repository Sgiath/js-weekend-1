import { makeActionCreator, makeAsyncActionCreator } from 'redux-toolbelt';

// Input form
export const fromInput = makeActionCreator('FROM_INPUT');
export const toInput = makeActionCreator('TO_INPUT');
export const dateInput = makeActionCreator('DATE_INPUT');

// Input suggestions
export const fromSuggestion = makeActionCreator('FROM_SUGGESTION');
export const toSuggestion = makeActionCreator('TO_SUGGESTION');

// API calls
export const findFlights = makeAsyncActionCreator('FIND_FLIGHTS');
export const findNextFlights = makeAsyncActionCreator('FIND_NEXT_FLIGHTS');
