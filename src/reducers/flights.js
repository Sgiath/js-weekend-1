import { findFlights, findNextFlights } from '../actions';

const initialState = {
  loading: false,
};

export default (state = initialState, { type, payload, meta }) => {
  switch (type) {
    case findFlights.TYPE:
      return { ...state, loading: true, data: undefined, meta: undefined };
    case findNextFlights.TYPE:
      return { ...state, loading: true };
    case findFlights.success.TYPE:
      return { ...state, loading: false, error: undefined, data: payload, meta: meta };
    case findNextFlights.success.TYPE:
      return { ...state, loading: false, error: undefined, data: state.data.concat(payload), meta: meta };
    case findFlights.failure.TYPE:
    case findNextFlights.failure.TYPE:
      return { ...state, loading: false, error: payload };
    default:
      return state;
  }
};
