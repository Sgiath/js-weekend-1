import { findFlights, findNextFlights } from '../actions';

export default (state = 1, { type }) => {
  switch (type) {
    case findFlights.success.TYPE:
      return 1;
    case findNextFlights.success.TYPE:
      return state + 1;
    default:
      return state;
  }
};
