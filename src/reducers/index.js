import inputForm from './inputForm';
import page from './page';
import flights from './flights';

export default {
  inputForm: inputForm,
  flights: flights,
  page: page,
};
