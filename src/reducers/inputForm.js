import { makeReducer, composeReducers } from 'redux-toolbelt';

import { fromInput, toInput, dateInput, fromSuggestion, toSuggestion } from '../actions';

export default composeReducers({
  from: {
    value: makeReducer(fromInput, { defaultState: '' }),
    suggestions: makeReducer(fromSuggestion),
  },
  to: {
    value: makeReducer(toInput, { defaultState: '' }),
    suggestions: makeReducer(toSuggestion),
  },
  date: makeReducer(dateInput, { defaultState: '' }),
});
