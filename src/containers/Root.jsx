import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { ConnectedFlagsProvider } from 'flag';

import App from './App';

class Root extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <ConnectedFlagsProvider>
            <App />
          </ConnectedFlagsProvider>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default Root;
