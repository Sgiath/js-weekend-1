import React from 'react';
import { Grid, GridCell } from 'rmwc/Grid';

import InputForm from './InputForm';
import FlightList from './FlightList';
import Settings from './Settings';
import { CenterCard } from '../components';

class App extends React.Component {
  render() {
    return (
      <Grid>
        <GridCell span={8}>
          <CenterCard title="Search for flight">
            <InputForm />
          </CenterCard>
          <FlightList />
        </GridCell>
        <GridCell span={4}>
          <Settings />
        </GridCell>
      </Grid>
    );
  }
}

export default App;
