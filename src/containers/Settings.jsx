import React from 'react';
import { connect } from 'react-redux';
import { setFlagsAction } from 'flag';
import { Switch } from 'rmwc/Switch';
import { Typography } from 'rmwc/Typography';

import { features } from '../selectors';

class Settings extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Typography use="title" tag="h1">
          Settings
        </Typography>
        <Switch
          checked={this.props.features.suggestions}
          onChange={(event) => this.props.setSuggestions(event.target.checked)}>
          Suggestions
        </Switch>
      </React.Fragment>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    features: features(state),
  }),
  (dispatch, ownProps) => ({
    setSuggestions: (value) => dispatch(setFlagsAction({ features: { suggestions: value } })),
  }),
)(Settings);
