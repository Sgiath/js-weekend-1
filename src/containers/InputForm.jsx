import React from 'react';
import { connect } from 'react-redux';
import { Flag } from 'flag';
import { Button } from 'rmwc/Button';

import { fromField, toField, dateField } from '../selectors';
import { fromInput, toInput, dateInput, findFlights } from '../actions';
import { InputField, InputFieldSuggestions } from '../components';

class InputForm extends React.Component {
  render() {
    return (
      <form onSubmit={this.props.onSubmit} action="#">
        <Flag
          name="features.suggestions"
          render={() => (
            <InputFieldSuggestions
              label="From"
              value={this.props.from.value}
              onChange={this.props.fromOnChange}
              suggestions={this.props.from.suggestions}
            />
          )}
          fallbackRender={() => (
            <InputField label="From" value={this.props.from.value} onChange={this.props.fromOnChange} />
          )}
        />
        <br />
        <Flag
          name="features.suggestions"
          render={() => (
            <InputFieldSuggestions
              label="To"
              value={this.props.to.value}
              onChange={this.props.toOnChange}
              suggestions={this.props.to.suggestions}
            />
          )}
          fallbackRender={() => <InputField label="To" value={this.props.to.value} onChange={this.props.toOnChange} />}
        />
        <br />
        <InputField label="Date" type="date" value={this.props.date} onChange={this.props.dateOnChange} />
        <br />
        <div style={{ float: 'right' }}>
          <Button
            onClick={(event) => {
              event.preventDefault();
              // TODO: check fields have correct values
              this.props.onSubmit();
            }}>
            Submit
          </Button>
        </div>
      </form>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    from: fromField(state),
    to: toField(state),
    date: dateField(state),
  }),
  (dispatch, ownProps) => ({
    fromOnChange: (value) => dispatch(fromInput(value)),
    toOnChange: (value) => dispatch(toInput(value)),
    dateOnChange: (value) => dispatch(dateInput(value)),
    onSubmit: () => dispatch(findFlights()),
  }),
)(InputForm);
