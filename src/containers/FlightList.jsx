import React from 'react';
import { connect } from 'react-redux';
import { List } from 'rmwc/List';
import { LinearProgress } from 'rmwc/LinearProgress';

import { FlightItem, DataComponent } from '../components';
import { flights } from '../selectors';
import { findNextFlights } from '../actions';

class FlightList extends React.Component {
  componentDidMount() {
    this.observer = new IntersectionObserver((entries) => {
      if (!this.props.flights.loading && this.props.flights.data && entries[0].isIntersecting) {
        this.props.loadNext();
      }
    });

    this.observer.observe(this.loading);
  }

  flightsRender = (data, { currency }) => (
    <List twoLine tag="ul">
      {data.map(({ id, price, route, fly_duration }) => (
        <FlightItem key={id} route={route} price={`${price} ${currency}`} duration={fly_duration} />
      ))}
    </List>
  );

  render() {
    return (
      <React.Fragment>
        <DataComponent render={this.flightsRender} {...this.props.flights} />
        <div ref={(ref) => (this.loading = ref)}>
          {this.props.flights.loading ? <LinearProgress determinate={false} /> : ''}
        </div>
      </React.Fragment>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    flights: flights(state),
  }),
  (dispatch, ownProps) => ({
    loadNext: () => dispatch(findNextFlights()),
  }),
)(FlightList);
