import axios from 'axios';

export const findFlightsPaginated = ({ from, to, date }, page, host) =>
  axios.get(`${host}/flights?flyFrom=${from}&to=${to}&dateFrom=${date}&offset=${5 * page}&limit=5&v=2&locale=en`);

export const suggestions = (term, host) => axios.get(`${host}/locations/?term=${term}&v=2&locale=en-US`);
