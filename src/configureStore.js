import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import { createFlagsReducer } from 'flag';

import reducers from './reducers';
import flagsConfig from './flags';

const configureStore = (history, sagaMiddleware) => {
  // List all middleware
  const middleware = [
    // React Router middleware
    routerMiddleware(history),

    // Saga middleware
    sagaMiddleware,
  ];

  // Redux Chrome extension for easy debugging in
  let composeEnhanced;
  if (!process.env.DISABLE_REDUX_STORE) {
    /* eslint-disable no-underscore-dangle */
    composeEnhanced = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    /* eslint-enable */
  } else {
    composeEnhanced = compose;
  }

  return createStore(
    combineReducers({
      ...reducers,
      router: routerReducer,
      flags: createFlagsReducer(flagsConfig),
    }),
    composeEnhanced(applyMiddleware(...middleware)),
  );
};

export default configureStore;
