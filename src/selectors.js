// Input form
export const fromField = (state) => state.inputForm.from;
export const toField = (state) => state.inputForm.to;
export const dateField = (state) => state.inputForm.date;

// Flags
export const restHost = (state) => state.flags.config.restHost;
export const features = (state) => state.flags.features;

// Flights
export const flights = (state) => state.flights;
export const page = (state) => state.page;

export const flightData = (state) => ({
  from: fromField(state).value,
  to: toField(state).value,
  date: dateField(state),
  page: page(state),
  restHost: restHost(state),
});
