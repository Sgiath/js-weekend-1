export default {
  features: {
    suggestions: false,
  },
  config: {
    api: 'rest', // graphql
    restHost: 'https://api.skypicker.com',
    graphqlHost: 'https://graphql.kiwi.com',
  },
};
