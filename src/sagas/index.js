import { all } from 'redux-saga/effects';

import findFlights from './findFlights';
import inputSuggestions from './inputSuggestions';

export default function*() {
  yield all([findFlights(), inputSuggestions()]);
}
