import { takeEvery, put, select, call, all } from 'redux-saga/effects';

import { findFlights, findNextFlights } from '../actions';
import { flightData } from '../selectors';
import { findFlightsPaginated } from '../api/rest';

function* findFlightsSaga({ type, payload, meta }) {
  try {
    const { restHost: host, date, from, to } = yield select(flightData);
    const response = yield call(
      findFlightsPaginated,
      {
        from,
        to,
        date: date
          .split('-')
          .reverse()
          .join('/'),
      },
      0,
      host,
    );
    yield put(findFlights.success(response.data.data, { ...response.data, data: undefined }));
    yield put(findNextFlights());
  } catch (error) {
    yield put(findFlights.failure(error));
  }
}

function* findNextFlightsSaga({ type, payload, meta }) {
  try {
    const { restHost: host, date, from, to, page } = yield select(flightData);
    const response = yield call(
      findFlightsPaginated,
      {
        from,
        to,
        date: date
          .split('-')
          .reverse()
          .join('/'),
      },
      page,
      host,
    );
    yield put(findNextFlights.success(response.data.data, { ...response.data, data: undefined }));
  } catch (error) {
    yield put(findNextFlights.failure(error));
  }
}

export default function*() {
  yield all([takeEvery(findFlights.TYPE, findFlightsSaga), takeEvery(findNextFlights.TYPE, findNextFlightsSaga)]);
}
