import { all, takeEvery, select, call, put } from 'redux-saga/effects';

import { fromInput, toInput, fromSuggestion, toSuggestion } from '../actions';
import { restHost } from '../selectors';
import { suggestions } from '../api/rest';

function* fetchSuggestions({ type, payload, meta }) {
  try {
    const host = yield select(restHost);
    const { data } = yield call(suggestions, payload, host);
    if (type === fromInput.TYPE) {
      yield put(fromSuggestion(data.locations));
    } else if (type === toInput.TYPE) {
      yield put(toSuggestion(data.locations));
    }
  } catch (error) {}
}

export default function*() {
  yield all([takeEvery(fromInput, fetchSuggestions), takeEvery(toInput, fetchSuggestions)]);
}
